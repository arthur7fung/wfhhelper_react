import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Contacts from "./components/contacts";
import NotFound from "./components/notFound";
import NavBar from "./components/navBar";
import LoginForm from "./components/loginForm";
import Logout from "./components/logout";
import ProtectedRoute from "./components/common/protectedRoute";
import auth from "./services/authService";
import { Helmet } from 'react-helmet'
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import Clock from "./components/clock";

class App extends Component {
  state = {};
  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user });
  }
  render() {
    const TITLE = 'Staff Contact';
    const { user } = this.state;
    return (
      <React.Fragment>
        <Helmet>
          <title>{ TITLE }</title>
        </Helmet>
        <ToastContainer />
        <NavBar user={user}/>
        <main className="container">
          <Switch>
            <Route path="/login" component={LoginForm} />
            <Route path="/logout" component={Logout} />
            <ProtectedRoute path="/phones" component={Contacts} />
            <ProtectedRoute path="/clock" component={Clock} />
            <Route path="/not-found" component={NotFound} />
            <Redirect from="/index.html" exact to="/phones/" />
            <Redirect from="/" exact to="/phones" />
            <Redirect to="/not-found" /> 
          </Switch>
         </main>
      </React.Fragment>
    );
  }
}

export default App;
