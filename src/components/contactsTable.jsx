import React, { Component } from "react";
import Table from "./common/table";

class ContactsTable extends Component {
  columns = [
    {
      path: "contactname",   label: "Staff Name"
    },
    { path: "phone", label: "Phone Number",
     content: contact =>
     <a href={`tel:${contact.phone}`} >{contact.phone}</a>
 }
];
  constructor() {
    super();
  }
 
  render() {
    const { Contacts, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={Contacts}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default ContactsTable;
