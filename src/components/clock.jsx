import React, { Component } from 'react';
import {onSendtoCtime} from "../services/clockService";
import { toast } from "react-toastify";

class Clock extends Component {
  state = {  }

handleCtime = async () => {
  try {
      var res = await onSendtoCtime();
      if(res.status === 200){
        toast.success("Clocked Successfully. Status: "+ res.status);
        }
        
      if(res.status === 401){
        toast.error("Clock Fail. Unauthorizad:"+ res.status);
        }
  } catch (ex) {
      if (ex.response && ex.response.status === 400)
        toast.error("Clock Fail. status 400");
      else
        toast.error("Clock Fail. An unexpected error occurrred.");
  }
};

  render() { 
    return (    
    <div>
    <h1>Clock your time</h1>
    <div className="row-container" >
    <div className="topleft">
        <h5>
            Please stay healthy and clock your time as same as you are in physical office. It is for the payroll record.<br/>
            Press the blue button for clocking a record<br/>
        </h5>
        <button onClick={() => this.handleCtime()} className="btn btn btn-primary">
            Clock
        </button>
    </div>
        <div className="topright">
            <b>!Attention: </b>
            <i>If you are in break, please clock out. Hardworking is appreciated.</i>
        </div>
    </div>
    </div> );
  }
}
export default Clock;
