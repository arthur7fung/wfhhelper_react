import React from "react";
import { Link, NavLink } from "react-router-dom";



const NavBar = ({ user }) => {
  const contactLink = `/phones`;
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/clock">
        CBA International
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
         <span className="navbar-toggler-icon" />
         {!user && (
              <NavLink className="nav-item nav-link" to="/login">
                Login
              </NavLink>
          )}
         {user && (
         <NavLink className="nav-item nav-link" to="/logout">
                Logout
              </NavLink>
         )}
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink className="nav-item nav-link" to={contactLink}>
            Staff Contact
          </NavLink>
          <NavLink className="nav-item nav-link" to="/clock">
          &nbsp; Clock
          </NavLink>
          {!user && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/login">
              &nbsp;   Login
              </NavLink>
            </React.Fragment>
          )}
          {user && (
            <React.Fragment>
              <NavLink className="nav-item nav-link" to="/">
              &nbsp;  {user.name}
              </NavLink>
              <NavLink className="nav-item nav-link" to="/logout">
              &nbsp;  Logout
              </NavLink>
            </React.Fragment>
          )} 
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
