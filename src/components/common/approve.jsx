import React from "react";

const Approve = props => {
  let classes = "fa fa-heart";
  if (!props.approved) classes += "-o";
  return (
    <i
      onClick={props.onClick}
      style={{ cursor: "pointer" }}
      className={classes}
      aria-hidden="true"
    />
  );
};

export default Approve;
