import React  from "react";

class Timer extends React.Component{
state = {sec: 0};
componentDidMount() {
  this.sec = setInterval( ()=>this.Counter(),1000
  )
}
Counter(){
    this.setState({  sec: this.state.sec+1 }); 
}

render(){
const { msg } = this.props;
return(
    <div>
    <p>{msg}Loading...({this.state.sec}s)</p>
    <div className="loader"></div> 
    </div>
)
}
}
export default Timer;
