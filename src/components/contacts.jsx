import React, { Component } from "react";
import { toast } from "react-toastify";
import ContactsTable from "./contactsTable";
import DeptListGroup from "./common/deptList";
import Pagination from "./common/pagination";
import Timer from "./common/timer";
import { getContact, deleteContact } from "../services/contactService";
import { getDepts } from "../services/deptService";
import { paginate } from "../utils/paginate";
import SearchBox from "./searchBox";
import _ from "lodash";

class Contacts extends Component {
  state = {
    Contacts: [],
    stores: [],
    currentPage: 1,
    pageSize: 20,
    searchQuery: "",
    selectedStore: null,
    sortColumn: { path: "contactname", order: "asc" },
    pagedata: 1
  };

  async componentDidMount() {
    const office =  [ ...getDepts()];
    const stores = [{ id: "ALL", name: "ALL" }, ...office];
    const { data: Contacts } = await getContact();
    const { length: count } = Contacts;
    if (count === 0) {
        const pagedata = 0;
        this.setState({pagedata});
    }
    this.setState({ Contacts, stores });
  }
 
  handleDelete = async contact => {
    const originalContacts = this.state.Contacts;
    const Contacts = originalContacts.filter(m => m.id !== contact.id);
    this.setState({ Contacts });
    try {
      await deleteContact(contact.id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        toast.error("This Contacts has already been deleted.");
        this.setState({ Contacts: originalContacts });
    }
  };

  handleApprove = contact => {
    const Contacts = [...this.state.Contacts];
    const index = Contacts.indexOf(contact);
    Contacts[index] = { ...Contacts[index] };
    Contacts[index].status = !Contacts[index].status;
    this.setState({ Contacts });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleStoreSelect = store => {
    this.setState({ selectedStore: store, searchQuery: "", currentPage: 1 });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, selectedStore: null, currentPage: 1 });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      sortColumn,
      selectedStore,
      searchQuery,
      Contacts: allContacts
    } = this.state;

    let filtered = allContacts;
    if (searchQuery)
      filtered = allContacts.filter(m =>
        m.contactname.toLowerCase().startsWith(searchQuery.toLowerCase())
      );
    else if (selectedStore && selectedStore.dept_id)
      filtered = allContacts.filter(m => m.dept === selectedStore.dept_id);
    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    const Contacts = paginate(sorted, currentPage, pageSize);
    return { totalCount: filtered.length, data: Contacts };
  };

  render() {
    const { length: count } = this.state.Contacts;
    const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
    const { user } = this.props;
    if (this.state.pagedata  === 0) return (   
    <p>There is no contacts under thisphone directory.</p>
    );
    if (count === 0){ 
      return (
        <Timer  msg="Please do not refresh this page! " />
      )
    }
    const { totalCount, data: Contacts } = this.getPagedData();

    return (
      <div className="row">
      <div className="col-4">
        <DeptListGroup
          items={this.state.stores}
          selectedItem={this.state.selectedStore}
          onItemSelect={this.handleStoreSelect}
        />
      </div>
      <div className="col">

        <p>Showing {totalCount} contacts in this phone directory.</p>

        <div className="row">
        <SearchBox value={searchQuery} onChange={this.handleSearch} />
        
        </div>

        <ContactsTable
          Contacts={Contacts}
          sortColumn={sortColumn}
          onApprove={this.handleApprove}
          onDelete={this.handleDelete}
          onSendResetEmail={this.handleSendResetEmail} 
          onSort={this.handleSort}
        />
        <Pagination
          itemsCount={totalCount}
          pageSize={pageSize}
          currentPage={currentPage}
          onPageChange={this.handlePageChange}
        />
      </div>
    </div>
    );
  }
}

export default Contacts;


