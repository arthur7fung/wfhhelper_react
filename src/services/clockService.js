import http from "./httpService";
import { apiUrl } from "../config.json";
import auth from "./authService";
const apiEndpoint = apiUrl + "/ctime";

function getClockingUrl(){
    var user = auth.getCurrentUser();
    var url = apiEndpoint  + "?username=" + user.NetCRMLogin;
    return url;
}

export function onSendtoCtime() {
    http.setBasicAuth('BasicAuthToken==');
    var resmsg = http.get(getClockingUrl());
    return resmsg;
  }