import http from "./httpService";
import { apiUrl } from "../config.json";
const apiEndpoint = apiUrl + "/staff";


function contactUrl() {
  return `${apiEndpoint}`;
}

export function getContact() {
  http.setBasicAuth('BasicAuthToken');
  return http.get(contactUrl());
}

export function deleteContact(contactId) {
  return http.delete(contactUrl(contactId));
}
