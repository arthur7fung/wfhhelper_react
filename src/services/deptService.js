export const Departments = [
  { id: "1", created_in: "PDEPT", dept_id: "IB&M" },
  { id: "2", created_in: "PDEPT", dept_id: "HR" },
  { id: "3", created_in: "PDEPT", dept_id: "SALES" },
  { id: "4", created_in: "PDEPT", dept_id: "OE" },
  { id: "5", created_in: "PDEPT", dept_id: "MKT" },
  { id: "6", created_in: "PDEPT", dept_id: "PURC" },
  { id: "7", created_in: "PDEPT", dept_id: "ACC" },
  { id: "8", created_in: "PDEPT", dept_id: "IT" }
];

export function getDepts() {
  return Departments.filter(g => g);
}